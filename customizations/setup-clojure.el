;;;;
;; Clojure
;;;;
;; A little more syntax highlighting
(require 'clojure-mode)
(require 'clojure-mode-extra-font-locking)
(require 'cider-mode)
(require 'cider)

;;
;; Clojure Mode Hooks
;;
(add-hook 'clojure-mode-hook 'enable-paredit-mode)
(add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)
;; This is useful for working with camel-case tokens, like names of
;; Java classes (e.g. JavaClassName)
(add-hook 'clojure-mode-hook 'subword-mode)
(add-hook 'clojure-mode-hook 'aggressive-indent-mode)
;; Autocomplete..
(add-hook 'clojure-mode-hook 'company-mode)

;; syntax hilighting for midje
;; We don't use midje, but leaving it here as an example of how to highlight certain symbols.
;; (add-hook 'clojure-mode-hook
;;           (lambda ()
;;             (setq inferior-lisp-program "lein repl")
;;             (font-lock-add-keywords
;;              nil
;;              '(("(\\(facts?\\)"
;;                 (1 font-lock-keyword-face))
;;                ("(\\(background?\\)"
;;                 (1 font-lock-keyword-face))))
;;             (define-clojure-indent (fact 1))
;;             (define-clojure-indent (facts 1))))

;;;;
;; Cider
;;;;

;; The following two lines are what's required to get cider-jack-in to work 
;; properly in Emacs 24.4 with Cider 0.16.0.

;; For some reason, the JAVA_CMD is being used by Emacs
(setenv "JAVA_CMD" "/opt/java/latest/bin/java")
(load-file "~/.emacs.d/elpa/seq-2.20/seq-24.el")

;; go right to the REPL buffer when it's finished connecting
(setq cider-repl-pop-to-buffer-on-connect t)

;; When there's a cider error, show its buffer and switch to it
(setq cider-show-error-buffer t)
(setq cider-auto-select-error-buffer t)

;; Where to store the cider history.
(setq cider-repl-history-file "~/.emacs.d/cider-history")

;; Wrap when navigating history.
(setq cider-repl-wrap-history t)
(setq cider-repl-user-pretty-printing t)
;;
;; Add the hooks for the Cider REPL here
;;
(add-hook 'cider-repl-mode-hook 'eldoc-mode)
(add-hook 'cider-repl-mode-hook 'company-mode)
(add-hook 'cider-repl-mode-hook 'paredit-mode)
(add-hook 'cider-repl-mode-hook 'rainbow-delimiters-mode)

;;
;; Add hooks for the Cider Mode here
;;
;; (add-hook 'cider-mode-hook 'eldoc-mode)
;; (add-hook 'cider-mode-hook 'company-mode)
;; (add-hook 'cider-mode-hook 'paredit-mode)
;; (add-hook 'cider-mode-hook 'rainbow-delimiters-mode)
;; (add-hook 'cider-mode-hook 'aggressive-indent-mode)

;; Use clojure mode for other extensions
(add-to-list 'auto-mode-alist '("\\.edn$" . clojure-mode))
(add-to-list 'auto-mode-alist '("\\.boot$" . clojure-mode))
(add-to-list 'auto-mode-alist '("\\.cljs.*$" . clojure-mode))
(add-to-list 'auto-mode-alist '("lein-env" . enh-ruby-mode))


(defun cider-test-chenpo-test-ns-fn (ns)
  "For a NS, return the test namespace, which may be the argument itself.
   We use a convention of applying the t-<ns> for test namespaces."
  (when ns
    (let* ((prefix "t-")
           (parsed (split-string ns "\\."))
           (package (butlast parsed))
           (ns (car  (last parsed))))
      (if (string-prefix-p prefix ns)
          (string-join (append  package (list ns)) ".")
        (string-join (append package (list  (concat prefix ns))) ".")))))

(setq cider-test-infer-test-ns 'cider-test-chenpo-test-ns-fn)

;;
;; Key Bindings
;;
;; I like to customize my repl so I can hit enter and get a newline rather than execute the command.
;;
(eval-after-load 'cider-mode
  '(progn 
     (define-key cider-repl-mode-map (kbd "RET") #'cider-repl-newline-and-indent)
     (define-key cider-repl-mode-map (kbd "C-<return>") #'cider-repl-return)
     (define-key cider-repl-mode-map (kbd "C-z") #'cider-switch-to-last-clojure-buffer)
     (define-key clojure-mode-map (kbd "C-z") #'cider-switch-to-repl-buffer)))

;; For all other modes
(global-set-key (kbd "C-z") #'cider-switch-to-repl-buffer)
(global-set-key (kbd "C-<f1>") 'previous-buffer)
(global-set-key (kbd "C-<f2>") 'next-buffer)
;;
;; These are key bindings that I wish to ELIMINATE when I'm in Clojure Mode.  I have no reason to use them
;; and when I accidently hit them, I flip out.
;;
;; This just removes the key-bindings you can always call the functions directly with M-x <function-name>
;;

(eval-after-load 'clojure-mode
  '(progn
     (define-key clojure-mode-map (kbd "M-k") #'ignore)
     (define-key clojure-mode-map (kbd "M-u") #'ignore)))

(eval-after-load 'cider-mode
  '(progn
     ;; This first one really screws up paredit by deleting all the parenthesis until it his an open parens.  
     (define-key cider-repl-mode-map (kbd "M-k") #'ignore)
     (define-key cider-repl-mode-map (kbd "M-u") #'ignore)))

;;
;; These are custom functions that the author of Clojure the Brave and True used.
;; I don't use these at the time, but they'll serve as useful examples for when we start automating
;; complex, repetitive, tasks.
;;
(defun cider-start-http-server ()
  (interactive)
  (cider-load-current-buffer)
  (let ((ns (cider-current-ns)))
    (cider-repl-set-ns ns)
    (cider-interactive-eval (format "(println '(def server (%s/start))) (println 'server)" ns))
    (cider-interactive-eval (format "(def server (%s/start)) (println server)" ns))))


(defun cider-refresh ()
  (interactive)
  (cider-interactive-eval (format "(user/reset)")))

(defun cider-user-ns ()
  (interactive)
  (cider-repl-set-ns "user"))

;;
;; Keeping this for reference.
;;
;; (eval-after-load 'cider
;;   '(progn
;;      (define-key clojure-mode-map (kbd "C-c C-v") 'cider-start-http-server)
;;      (define-key clojure-mode-map (kbd "C-M-r") 'cider-refresh)
;;      (define-key clojure-mode-map (kbd "C-c u") 'cider-user-ns)
;;      (define-key cider-mode-map (kbd "C-c u") 'cider-user-ns)))
